//
//  ViewController.swift
//  SwiftyJSONEx
//
//  Created by admin on 4/28/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyButton
class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: *** Local variables
    let imagePicker = UIImagePickerController()
    let todoEndpoint: String = "https://api.imagga.com/v1/tagging?url=http://imagga.com/static/images/tagging/wind-farm-538576_640.jpg"
    let imageUploadContent = "https://api.imagga.com/v1/content"
    let headers = [
        "Authorization": "Basic YWNjXzhjMjljMzA5NjdjZjdmYjpkYzE2MjlmODcxNzExYmNiMGFmNmQ2ZmE4MGQ0NzZkZg=="
    ]
    
    
    
    // MARK: *** Data Models
    
    // MARK: *** UI Elements
    @IBOutlet weak var getImage: FlatButton!
    @IBOutlet weak var imageView: UIImageView!
    // MARK: *** UI Events
    @IBAction func getImageButtonTapped(_ sender: Any) {
        present(imagePicker, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUIForButton()
        imagePicker.delegate = self
        imageView.isHidden = true
        //        Alamofire.request(todoEndpoint, method: .get, headers: headers).responseJSON{ response in
        //            guard response.result.isSuccess else {
        //                print("Error while fetching tags")
        //                return
        //            }
        //            guard let responseJSON = response.result.value as? AnyObject else{
        //                print("Invalid tag information received from the service")
        //                return
        //            }
        //            guard let results = responseJSON["results"] as? [AnyObject] else {
        //                print("Invalid results information")
        //                return
        //            }
        //            guard let tags = results[0]["tags"] as? [AnyObject] else {
        //                print("Invalid results information")
        //                return
        //            }
        //            for i in 0...tags.count-1{
        //                guard let tag = tags[i]["tag"] as? String else {
        //                    print("Could not get tag from JSON")
        //                    return
        //                }
        //                print(tag)
        //            }
        //        }
    }
    
    func initUIForButton(){
        let color = UIColor(rgb: 0xE3E3E3)
        getImage.color = color
        let colorHilighted = UIColor(rgb: 0x455D7A)
        getImage.highlightedColor = colorHilighted
    }
    
    // MARK: *** UIImagePicker
    var selectedImages : UIImage? = nil
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.image = pickedImage
            selectedImages = pickedImage
            
            guard let imageData = UIImageJPEGRepresentation(pickedImage, 1) else {
                print("Could not get JPEG representation of UIImage")
                return
            }
            
            var statusUploadImage = false
            var idImage = ""
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(imageData, withName: "photo", fileName: "photo.jpeg", mimeType: "image/jpeg")
            }, to: imageUploadContent, headers: headers, encodingCompletion: { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        //print(response)
                        if let info = response.result.value as? Dictionary<String, AnyObject> {
                            if let status = info["status"] as? String{
                                if status == "success"{
                                    statusUploadImage = true
                                }
                            }
                            if statusUploadImage == true{
                                let uploaded = info["uploaded"] as? [AnyObject]
                                idImage = uploaded![0]["id"] as! String
                                print(idImage)
                            }
                        }
                    }
                    break
                case .failure(let encodingError):
                    print("Error")
                    break
                }
            })
            
            
            
            imageView.isHidden = false
            dismiss(animated: true)
        }
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true)
        }
    }}
